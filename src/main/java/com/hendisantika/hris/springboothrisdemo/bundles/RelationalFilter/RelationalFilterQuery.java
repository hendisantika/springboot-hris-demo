package com.hendisantika.hris.springboothrisdemo.bundles.RelationalFilter;

import com.hendisantika.hris.springboothrisdemo.core.common.ModelSpecification;
import com.hendisantika.hris.springboothrisdemo.core.main.ModelQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-27
 * Time: 07:55
 */
@Component
public class RelationalFilterQuery extends ModelQuery<RelationalFilter> {
    @Autowired
    protected RelationalFilterRepository relationalFilterRepository;
    private ModelSpecification<RelationalFilter> spec;

    public RelationalFilterQuery(RelationalFilterRepository RelationalFilterRepository) {
        this.relationalFilterRepository = RelationalFilterRepository;
    }

    public RelationalFilter getRelationalFilterById(Long id) {
        return relationalFilterRepository.getOne(id);
    }

    public List<RelationalFilter> RelationalFilters(String where, String sort) {
        return query(where, spec, relationalFilterRepository, sort);
    }
}