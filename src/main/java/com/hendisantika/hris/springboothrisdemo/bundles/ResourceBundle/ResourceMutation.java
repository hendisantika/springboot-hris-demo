package com.hendisantika.hris.springboothrisdemo.bundles.ResourceBundle;

import com.hendisantika.hris.springboothrisdemo.core.main.ModelMutation;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-27
 * Time: 07:57
 */
@Component
public class ResourceMutation extends ModelMutation<Resource> {

    public ResourceMutation(ResourceRepository resourceRepository) {
        this.resourceRepository = resourceRepository;
    }


    public Resource newResource(String uid, String name, String description, Boolean isgenerating, String messagelog) {
        Resource resource = new Resource(uid, name, description, isgenerating, messagelog);

        resourceRepository.save(resource);
        return resource;
    }

    public Boolean deleteResource(Long id) {
        return deleteModel(id, resourceRepository);
    }

    public Resource updateResource(Long id, String uid, String name, String description, Boolean isgenerating, String messagelog) {
        Resource resource = resourceRepository.getOne(id);

        if (uid != null)
            resource.setUid(uid);

        if (name != null)
            resource.setName(name);

        if (description != null)
            resource.setDescription(description);

        if (isgenerating != null)
            resource.setIsGenerating(isgenerating);

        if (messagelog != null)
            resource.setMessageLog(messagelog);

        return resource;
    }
}
