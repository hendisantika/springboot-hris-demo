package com.hendisantika.hris.springboothrisdemo.bundles.FieldBundle;

import com.hendisantika.hris.springboothrisdemo.core.common.ModelSpecification;
import com.hendisantika.hris.springboothrisdemo.core.main.ModelQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-24
 * Time: 07:21
 */
@Component
public class FieldQuery extends ModelQuery<Field> {
    @Autowired
    protected FieldRepository fieldRepository;
    private ModelSpecification<Field> spec;

    public FieldQuery(FieldRepository fieldRepository) {
        this.fieldRepository = fieldRepository;
    }

    public Field getFieldById(Long id) {
        return fieldRepository.getOne(id);
    }


    public List<Field> Fields(String where, String sort) {
        return query(where, spec, fieldRepository, sort);
    }

}