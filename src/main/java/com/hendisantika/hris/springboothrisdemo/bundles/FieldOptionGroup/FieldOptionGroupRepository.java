package com.hendisantika.hris.springboothrisdemo.bundles.FieldOptionGroup;

import com.hendisantika.hris.springboothrisdemo.core.main.ModelRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-22
 * Time: 22:03
 */
@Repository
public interface FieldOptionGroupRepository extends ModelRepository<FieldOptionGroup> {
}