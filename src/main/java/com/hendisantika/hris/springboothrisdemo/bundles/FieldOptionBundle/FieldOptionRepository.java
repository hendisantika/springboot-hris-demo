package com.hendisantika.hris.springboothrisdemo.bundles.FieldOptionBundle;

import com.hendisantika.hris.springboothrisdemo.core.main.ModelRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-22
 * Time: 22:03
 */
public interface FieldOptionRepository extends ModelRepository<FieldOption> {
}