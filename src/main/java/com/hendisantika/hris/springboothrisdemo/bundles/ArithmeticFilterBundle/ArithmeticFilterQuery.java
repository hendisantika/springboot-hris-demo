package com.hendisantika.hris.springboothrisdemo.bundles.ArithmeticFilterBundle;

import com.hendisantika.hris.springboothrisdemo.core.common.ModelSpecification;
import com.hendisantika.hris.springboothrisdemo.core.main.ModelQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-24
 * Time: 07:18
 */
@Component
public class ArithmeticFilterQuery extends ModelQuery<ArithmeticFilter> {
    @Autowired
    private ArithmeticFilterRepository arithmeticFilterRepository;

    private ModelSpecification<ArithmeticFilter> spec;

    public ArithmeticFilterQuery(ArithmeticFilterRepository arithmeticFilterRepository) {
        this.arithmeticFilterRepository = arithmeticFilterRepository;
    }

    public ArithmeticFilter getArithmeticFilterById(Long id) {
        return arithmeticFilterRepository.getOne(id);
    }

    public List<ArithmeticFilter> Arithmeticfilter(String where, String sort) {
        return query(where, spec, arithmeticFilterRepository, sort);
    }
}