package com.hendisantika.hris.springboothrisdemo.bundles.FieldGroupSetBundle;

import com.hendisantika.hris.springboothrisdemo.core.common.ModelSpecification;
import com.hendisantika.hris.springboothrisdemo.core.main.ModelQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-25
 * Time: 07:05
 */
@Component
public class FieldGroupSetQuery extends ModelQuery<FieldGroupSet> {
    @Autowired
    protected FieldGroupSetRepository fieldGroupSetRepository;

    private ModelSpecification<FieldGroupSet> spec;

    public FieldGroupSetQuery(FieldGroupSetRepository fieldGroupSetRepository) {
        this.fieldGroupSetRepository = fieldGroupSetRepository;
    }

    public FieldGroupSet getFieldGroupSetsById(Long id) {
        return fieldGroupSetRepository.getOne(id);
    }

    public List<FieldGroupSet> FieldGroupSets(String where, String sort) {
        return query(where, spec, fieldGroupSetRepository, sort);
    }
}