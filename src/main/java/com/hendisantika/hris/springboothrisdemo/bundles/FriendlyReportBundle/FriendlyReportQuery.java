package com.hendisantika.hris.springboothrisdemo.bundles.FriendlyReportBundle;

import com.hendisantika.hris.springboothrisdemo.core.common.ModelSpecification;
import com.hendisantika.hris.springboothrisdemo.core.main.ModelQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-26
 * Time: 07:15
 */
@Component
public class FriendlyReportQuery extends ModelQuery<FriendlyReport> {
    @Autowired
    private FriendlyReportRepository friendlyReportRepository;

    private ModelSpecification<FriendlyReport> spec;

    public FriendlyReportQuery(FriendlyReportRepository friendlyReportRepository) {
        this.friendlyReportRepository = friendlyReportRepository;
    }


    public FriendlyReport getFriendlyReportById(Long id) {
        return friendlyReportRepository.getOne(id);
    }

    public List<FriendlyReport> FriendlyReport(String where, String sort) {
        return query(where, spec, friendlyReportRepository, sort);
    }

}
