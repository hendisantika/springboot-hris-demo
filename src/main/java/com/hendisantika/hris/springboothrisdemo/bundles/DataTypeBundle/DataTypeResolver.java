package com.hendisantika.hris.springboothrisdemo.bundles.DataTypeBundle;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-24
 * Time: 07:21
 */
@Component
public class DataTypeResolver implements GraphQLResolver<DataType> {
}