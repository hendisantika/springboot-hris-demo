package com.hendisantika.hris.springboothrisdemo.bundles.RelationalFilter;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.hendisantika.hris.springboothrisdemo.bundles.FieldBundle.Field;
import com.hendisantika.hris.springboothrisdemo.bundles.FieldBundle.FieldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-27
 * Time: 07:56
 */
@Component
public class RelationalFilterResolver implements GraphQLResolver<RelationalFilter> {
    @Autowired
    private RelationalFilterRepository relationFilterRepository;

    @Autowired
    private FieldRepository fieldRepository;

    public RelationalFilterResolver(RelationalFilterRepository relationFilterRepository, FieldRepository fieldRepository) {
        this.relationFilterRepository = relationFilterRepository;
        this.fieldRepository = fieldRepository;
    }

    public Field getField(RelationalFilter relationalFilter) {
        return fieldRepository.getOne(relationalFilter.getField().getId());
    }

}