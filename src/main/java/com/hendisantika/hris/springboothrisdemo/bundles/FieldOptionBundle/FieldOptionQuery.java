package com.hendisantika.hris.springboothrisdemo.bundles.FieldOptionBundle;

import com.hendisantika.hris.springboothrisdemo.core.common.ModelSpecification;
import com.hendisantika.hris.springboothrisdemo.core.main.ModelQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-25
 * Time: 07:07
 */
@Component
public class FieldOptionQuery extends ModelQuery<FieldOption> {
    @Autowired
    protected FieldOptionRepository fieldOptionRepository;

    private ModelSpecification<FieldOption> spec;

    public FieldOptionQuery(FieldOptionRepository fieldOptionRepository) {
        this.fieldOptionRepository = fieldOptionRepository;
    }

    public FieldOption getFieldOptionById(Long id) {
        return fieldOptionRepository.getOne(id);
    }

    public List<FieldOption> FieldOptions(String where, String sort) {
        return query(where, spec, fieldOptionRepository, sort);
    }
}