package com.hendisantika.hris.springboothrisdemo.bundles.DashboardChartBundle;

import com.hendisantika.hris.springboothrisdemo.core.common.ModelSpecification;
import com.hendisantika.hris.springboothrisdemo.core.main.ModelQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-24
 * Time: 07:19
 */
@Component
public class DashboardChartQuery extends ModelQuery<DashboardChart> {

    @Autowired
    private DashboardChartRepository dashboardChartRepository;

    private ModelSpecification<DashboardChart> spec;

    public DashboardChartQuery(DashboardChartRepository dashboardChartRepository) {
        this.dashboardChartRepository = dashboardChartRepository;
    }

    public DashboardChart getDashboardChartById(Long id) {
        return dashboardChartRepository.getOne(id);
    }

    public List<DashboardChart> DashboardCharts(String where, String sort) {
        return query(where, spec, dashboardChartRepository, sort);
    }
}