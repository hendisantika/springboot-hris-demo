package com.hendisantika.hris.springboothrisdemo.bundles.RecordValue;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.hendisantika.hris.springboothrisdemo.bundles.RecordBundle.RecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-27
 * Time: 07:54
 */
@Component
public class RecordValueResolver implements GraphQLResolver<RecordValue> {
    @Autowired
    RecordValueRepository recordValueRepository;

    @Autowired
    RecordRepository recordRepository;

    public RecordValueResolver(RecordRepository recordRepository, RecordValueRepository recordValueRepository) {
        this.recordRepository = recordRepository;
        this.recordValueRepository = recordValueRepository;
    }
}