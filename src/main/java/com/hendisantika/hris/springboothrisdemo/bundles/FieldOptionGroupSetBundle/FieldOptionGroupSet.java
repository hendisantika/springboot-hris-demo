package com.hendisantika.hris.springboothrisdemo.bundles.FieldOptionGroupSetBundle;

import com.hendisantika.hris.springboothrisdemo.core.main.Model;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-23
 * Time: 07:17
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "fieldOptionGroupSet")
public class FieldOptionGroupSet extends Model {
    private String name;
    private String description;

    public FieldOptionGroupSet() {
        super();
    }

    public FieldOptionGroupSet(String uid, String name, String description) {
        super();
        this.name = name;
        this.description = description;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
