package com.hendisantika.hris.springboothrisdemo.bundles.FieldOptionMergeBundle;

import com.hendisantika.hris.springboothrisdemo.core.common.ModelSpecification;
import com.hendisantika.hris.springboothrisdemo.core.main.ModelQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-26
 * Time: 07:11
 */
@Component
public class FieldOptionMergeQuery extends ModelQuery<FieldOptionMerge> {
    @Autowired
    private FieldOptionMergeRepository fieldOptionMergeRepository;
    private ModelSpecification<FieldOptionMerge> spec;

    public FieldOptionMergeQuery(FieldOptionMergeRepository fieldOptionMergeRepository) {
        this.fieldOptionMergeRepository = fieldOptionMergeRepository;
    }

    public FieldOptionMerge getFieldOptionMergeById(Long id) {
        return fieldOptionMergeRepository.getOne(id);
    }

    public List<FieldOptionMerge> queryFieldOptionMerge(String where, String sort) {
        return query(where, spec, fieldOptionMergeRepository, sort);
    }
}