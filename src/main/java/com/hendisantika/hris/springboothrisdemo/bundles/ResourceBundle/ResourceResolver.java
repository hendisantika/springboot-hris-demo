package com.hendisantika.hris.springboothrisdemo.bundles.ResourceBundle;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-27
 * Time: 07:59
 */
@Component
public class ResourceResolver implements GraphQLResolver<Resource> {
    @Autowired
    private ResourceRepository resourceRepository;

    public ResourceResolver(ResourceRepository resourceRepository) {
        this.resourceRepository = resourceRepository;
    }
}