package com.hendisantika.hris.springboothrisdemo.bundles.FieldBundle;

import com.hendisantika.hris.springboothrisdemo.core.main.ModelRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-23
 * Time: 07:07
 */
@Repository
public interface FieldRepository extends ModelRepository<Field> {

}