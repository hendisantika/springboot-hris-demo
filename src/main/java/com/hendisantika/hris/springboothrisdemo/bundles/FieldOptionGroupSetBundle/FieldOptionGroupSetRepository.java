package com.hendisantika.hris.springboothrisdemo.bundles.FieldOptionGroupSetBundle;

import com.hendisantika.hris.springboothrisdemo.core.main.ModelRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-23
 * Time: 07:18
 */
@Repository
public interface FieldOptionGroupSetRepository extends ModelRepository<FieldOptionGroupSet> {
}