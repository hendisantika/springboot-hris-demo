package com.hendisantika.hris.springboothrisdemo.bundles.InputTypeBundle;

import com.hendisantika.hris.springboothrisdemo.core.main.Model;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-21
 * Time: 18:49
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "inputType")
public class InputType extends Model {
    private String name;
    private String description;
    private String htmltag;

    public InputType() {
        super();
    }

    public InputType(Long id) {
        super();
        this.id = id;
    }

    public InputType(String uid, String name, String description, String htmltag) {
        super();
        this.name = name;
        this.description = description;
        this.htmltag = htmltag;
    }


    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHtmltag() {
        return htmltag;
    }

    public void setHtmltag(String htmltag) {
        this.htmltag = htmltag;
    }

}
