package com.hendisantika.hris.springboothrisdemo.bundles.FieldOptionGroup;

import com.hendisantika.hris.springboothrisdemo.core.common.ModelSpecification;
import com.hendisantika.hris.springboothrisdemo.core.main.ModelQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-25
 * Time: 07:09
 */
@Component
public class FieldOptionGroupQuery extends ModelQuery<FieldOptionGroup> {
    @Autowired
    protected FieldOptionGroupRepository fieldOptionGroupRepository;

    private ModelSpecification<FieldOptionGroup> spec;

    public FieldOptionGroupQuery(FieldOptionGroupRepository fieldOptionGroupRepository) {
        this.fieldOptionGroupRepository = fieldOptionGroupRepository;
    }

    public FieldOptionGroup getFieldOptionGroupById(Long id) {
        return fieldOptionGroupRepository.getOne(id);
    }

    public List<FieldOptionGroup> FieldOptionGroup(String where, String sort) {
        return query(where, spec, fieldOptionGroupRepository, sort);
    }

}
