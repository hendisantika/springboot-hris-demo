package com.hendisantika.hris.springboothrisdemo.bundles.FieldGroupSetBundle;

import com.hendisantika.hris.springboothrisdemo.core.main.ModelMutation;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-25
 * Time: 07:05
 */
@Component
public class FieldGroupSetMutation extends ModelMutation<FieldGroupSet> {

    public FieldGroupSetMutation(FieldGroupSetRepository fieldGroupSetRepository) {
        this.fieldGroupSetRepository = fieldGroupSetRepository;
    }

    public FieldGroupSet newFieldGroupSet(String uid, String name, String description) {
        FieldGroupSet fieldGroupSet = new FieldGroupSet(uid, name, description);

        fieldGroupSetRepository.save(fieldGroupSet);
        return fieldGroupSet;
    }

    public Boolean deleteFieldGroupSet(Long id) {
        return deleteModel(id, fieldGroupSetRepository);
    }

    public FieldGroupSet updateFieldGroupSet(Long id, String uid, String name, String description) {
        FieldGroupSet fieldGroupSet = fieldGroupSetRepository.getOne(id);

        if (uid != null)
            fieldGroupSet.setUid(uid);

        if (name != null)
            fieldGroupSet.setName(name);

        if (description != null)
            fieldGroupSet.setDescription(description);

        fieldGroupSetRepository.save(fieldGroupSet);
        return fieldGroupSet;
    }

}