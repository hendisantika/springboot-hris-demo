package com.hendisantika.hris.springboothrisdemo.bundles.DataTypeBundle;

import com.hendisantika.hris.springboothrisdemo.core.common.ModelSpecification;
import com.hendisantika.hris.springboothrisdemo.core.main.ModelQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-24
 * Time: 07:20
 */

@Component
public class DataTypeQuery extends ModelQuery<DataType> {
    @Autowired
    private DataTypeRepository dataTypeRepository;

    private ModelSpecification<DataType> spec;

    public DataTypeQuery(DataTypeRepository dataTypeRepository) {
        this.dataTypeRepository = dataTypeRepository;
    }

    public DataType getDataTypeById(Long id) {
        return dataTypeRepository.getOne(id);
    }

    public List<DataType> DataType(String where, String sort) {
        return query(where, spec, dataTypeRepository, sort);
    }
}