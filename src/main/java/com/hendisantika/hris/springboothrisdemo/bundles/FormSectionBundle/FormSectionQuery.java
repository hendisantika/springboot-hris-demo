package com.hendisantika.hris.springboothrisdemo.bundles.FormSectionBundle;

import com.hendisantika.hris.springboothrisdemo.core.common.ModelSpecification;
import com.hendisantika.hris.springboothrisdemo.core.main.ModelQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-26
 * Time: 07:14
 */
@Component
public class FormSectionQuery extends ModelQuery<FormSection> {
    @Autowired
    private FormSectionRepository formSectionRepository;

    private ModelSpecification<FormSection> spec;

    public FormSectionQuery(FormSectionRepository formSectionRepository) {
        this.formSectionRepository = formSectionRepository;
    }

    public FormSection getFormSectionById(Long id) {
        return formSectionRepository.getOne(id);
    }

    public List<FormSection> FormSections(String where, String sort) {
        return query(where, spec, formSectionRepository, sort);
    }
}