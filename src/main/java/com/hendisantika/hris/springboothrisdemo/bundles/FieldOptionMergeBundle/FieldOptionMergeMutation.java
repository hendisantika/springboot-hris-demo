package com.hendisantika.hris.springboothrisdemo.bundles.FieldOptionMergeBundle;

import com.hendisantika.hris.springboothrisdemo.core.main.ModelMutation;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-26
 * Time: 07:10
 */
@Component
public class FieldOptionMergeMutation extends ModelMutation<FieldOptionMerge> {

    public FieldOptionMergeMutation(FieldOptionMergeRepository fieldOptionMergeRepository) {
        this.fieldOptionMergeRepository = fieldOptionMergeRepository;
    }

    public FieldOptionMerge newFieldOptionMerge(Integer fieldId, Integer mergedfieldoptionId, String uid, String removedfieldoptionvalue, String removedfieldoptionuid) {
        FieldOptionMerge fieldOptionMerge = new FieldOptionMerge(fieldId, mergedfieldoptionId, uid, removedfieldoptionvalue, removedfieldoptionuid);

        fieldOptionMergeRepository.save(fieldOptionMerge);
        return fieldOptionMerge;
    }

    public Boolean deleteFieldOptionMerge(Long id) {
        return deleteModel(id, fieldOptionMergeRepository);
    }

    public FieldOptionMerge updateFieldOptionMerge(Long id, Integer fieldId, Integer mergedfieldoptionId, String uid, String removedfieldoptionvalue, String removedfieldoptionuid) {
        FieldOptionMerge fieldOptionMerge = fieldOptionMergeRepository.getOne(id);

        if (fieldId != null)
            fieldOptionMerge.setFieldId(fieldId);

        if (mergedfieldoptionId != null)
            fieldOptionMerge.setMergedfieldoptionId(mergedfieldoptionId);

        if (uid != null)
            fieldOptionMerge.setUid(uid);

        if (removedfieldoptionvalue != null)
            fieldOptionMerge.setRemovedfieldoptionvalue(removedfieldoptionvalue);

        if (removedfieldoptionuid != null)
            fieldOptionMerge.setRemovedfieldoptionuid(removedfieldoptionuid);

        fieldOptionMergeRepository.save(fieldOptionMerge);
        return fieldOptionMerge;
    }
//
//    newFieldOptionMerge(fieldId:String!,
//                        mergedfieldoptionId:String!,
//                        uid:String!,
//                        removedfieldoptionvalue:String!,
//                        removedfieldoptionuid:String!):FieldOptionMerge!
//
//    deleteFieldOptionMerge(id:ID!):String
//
//    updateFieldOptionMerge(id:ID!, fieldId:String,
//                           mergedfieldoptionId:String,
//                           uid:String,
//                           removedfieldoptionvalue:String,
//                           removedfieldoptionuid:String):FieldOptionMerge
}