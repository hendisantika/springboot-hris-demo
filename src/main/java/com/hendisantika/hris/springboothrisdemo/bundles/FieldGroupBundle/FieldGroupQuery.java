package com.hendisantika.hris.springboothrisdemo.bundles.FieldGroupBundle;

import com.hendisantika.hris.springboothrisdemo.core.common.ModelSpecification;
import com.hendisantika.hris.springboothrisdemo.core.main.ModelQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-24
 * Time: 07:22
 */
@Component
public class FieldGroupQuery extends ModelQuery<FieldGroup> {
    @Autowired
    protected FieldGroupRepository fieldGroupRepository;

    private ModelSpecification<FieldGroup> spec;

    public FieldGroupQuery(FieldGroupRepository fieldGroupRepository) {
        this.fieldGroupRepository = fieldGroupRepository;
    }

    public FieldGroup getFieldGroupById(Long id) {
        return fieldGroupRepository.getOne(id);
    }

    public List<FieldGroup> FieldGroups(String where, String sort) {
        return query(where, spec, fieldGroupRepository, sort);
    }

}