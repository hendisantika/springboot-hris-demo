package com.hendisantika.hris.springboothrisdemo.bundles.DashboardChartBundle;

import com.hendisantika.hris.springboothrisdemo.core.main.ModelMutation;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-23
 * Time: 07:24
 */
@Component
public class DashboardChartMutation extends ModelMutation<DashboardChart> {

    public DashboardChartMutation(DashboardChartRepository dashboardChartRepository) {
        this.dashboardChartRepository = dashboardChartRepository;
    }

    public DashboardChart newDashboardChart(Integer fieldoneId, Integer fieldtwoId, Integer userId, String name, String description, String graphtype, Boolean lowerlevels, Boolean systemwide) {
        DashboardChart dashboardChart = new DashboardChart(fieldoneId, fieldtwoId, userId, name, description, graphtype, lowerlevels, systemwide);

        dashboardChartRepository.save(dashboardChart);
        return dashboardChart;
    }

    public Boolean deleteDashboardChart(Long id) {
        return deleteModel(id, dashboardChartRepository);
    }

    public DashboardChart updateDashboardChart(Long id, Integer fieldoneId, Integer fieldtwoId, Integer userId, String name, String description, String graphtype, Boolean lowerlevels, Boolean systemwide) {
        DashboardChart dashboardChart = dashboardChartRepository.getOne(id);

        if (fieldoneId != null)
            dashboardChart.setFieldoneId(fieldoneId);

        if (fieldtwoId != null)
            dashboardChart.setFieldtwoId(fieldtwoId);

        if (userId != null)
            dashboardChart.setUserId(userId);

        if (name != null)
            dashboardChart.setName(name);

        if (description != null)
            dashboardChart.setDescription(description);

        if (graphtype != null)
            dashboardChart.setGraphtype(graphtype);

        if (lowerlevels != null)
            dashboardChart.setLowerlevels(lowerlevels);

        if (systemwide != null)
            dashboardChart.setSystemwide(systemwide);

        dashboardChartRepository.save(dashboardChart);
        return dashboardChart;
    }
}