package com.hendisantika.hris.springboothrisdemo.bundles.RecordValue;

import com.hendisantika.hris.springboothrisdemo.bundles.FieldBundle.Field;
import com.hendisantika.hris.springboothrisdemo.bundles.RecordBundle.Record;
import com.hendisantika.hris.springboothrisdemo.core.main.Model;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-22
 * Time: 19:48
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "record_value")
public class RecordValue extends Model {

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "record_id", nullable = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Record record;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "field_id", nullable = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Field field;

    private String value;

    public RecordValue() {
        super();
    }

    public RecordValue(String value) {
        super();
        this.value = value;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public Record getRecord() {
        return record;
    }

    public void setRecord(Record record) {
        this.record = record;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
