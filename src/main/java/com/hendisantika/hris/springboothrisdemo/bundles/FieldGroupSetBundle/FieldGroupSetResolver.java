package com.hendisantika.hris.springboothrisdemo.bundles.FieldGroupSetBundle;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-25
 * Time: 07:06
 */
@Component
public class FieldGroupSetResolver implements GraphQLResolver<FieldGroupSet> {
    @Autowired
    private FieldGroupSetRepository fieldGroupSetRepository;

    public FieldGroupSetResolver(FieldGroupSetRepository fieldGroupSetRepository) {
        this.fieldGroupSetRepository = fieldGroupSetRepository;
    }
}