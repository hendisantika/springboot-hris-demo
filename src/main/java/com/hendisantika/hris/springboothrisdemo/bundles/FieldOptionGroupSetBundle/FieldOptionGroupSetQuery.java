package com.hendisantika.hris.springboothrisdemo.bundles.FieldOptionGroupSetBundle;

import com.hendisantika.hris.springboothrisdemo.core.common.ModelSpecification;
import com.hendisantika.hris.springboothrisdemo.core.main.ModelQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-25
 * Time: 07:11
 */
@Component
public class FieldOptionGroupSetQuery extends ModelQuery<FieldOptionGroupSet> {
    @Autowired
    protected FieldOptionGroupSetRepository fieldOptionGroupSetRepository;

    private ModelSpecification<FieldOptionGroupSet> spec;

    public FieldOptionGroupSetQuery(FieldOptionGroupSetRepository fieldOptionGroupSetRepository) {
        this.fieldOptionGroupSetRepository = fieldOptionGroupSetRepository;
    }

    public FieldOptionGroupSet getFieldOptionGroupSetById(Long id) {
        return fieldOptionGroupSetRepository.getOne(id);
    }

    public List<FieldOptionGroupSet> FieldOptionGroupSet(String where, String sort) {
        return query(where, spec, fieldOptionGroupSetRepository, sort);
    }
}