package com.hendisantika.hris.springboothrisdemo.bundles.ResourceBundle;

import com.hendisantika.hris.springboothrisdemo.core.common.ModelSpecification;
import com.hendisantika.hris.springboothrisdemo.core.main.ModelQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-27
 * Time: 07:59
 */
@Component
public class ResourceQuery extends ModelQuery<Resource> {
    @Autowired
    ResourceRepository resourceRepository;

    private ModelSpecification<Resource> spec;

    public ResourceQuery(ResourceRepository resourceRepository) {
        this.resourceRepository = resourceRepository;
    }

    public Resource getResourceById(Long id) {
        return resourceRepository.getOne(id);
    }

    public List<Resource> Resources(String where, String sort) {
        return query(where, spec, resourceRepository, sort);
    }
}