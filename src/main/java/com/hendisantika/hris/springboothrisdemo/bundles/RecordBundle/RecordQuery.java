package com.hendisantika.hris.springboothrisdemo.bundles.RecordBundle;

import com.hendisantika.hris.springboothrisdemo.core.common.ModelSpecification;
import com.hendisantika.hris.springboothrisdemo.core.main.ModelQuery;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-27
 * Time: 07:52
 */
@Component
public class RecordQuery extends ModelQuery<Record> {
    private ModelSpecification<Record> spec;

    public RecordQuery(RecordRepository recordRepository) {
        this.recordRepository = recordRepository;
    }

    public List<Record> Record(String where, String sort) {
        return query(where, spec, recordRepository, sort);
    }

}