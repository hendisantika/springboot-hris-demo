package com.hendisantika.hris.springboothrisdemo.bundles.FieldOptionBundle;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.hendisantika.hris.springboothrisdemo.bundles.FieldBundle.Field;
import com.hendisantika.hris.springboothrisdemo.bundles.FieldBundle.FieldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-25
 * Time: 07:07
 */
@Component
public class FieldOptionResolver implements GraphQLResolver<FieldOption> {
    @Autowired
    private FieldOptionRepository fieldOptionRepository;

    @Autowired
    private FieldRepository fieldRepository;

    public FieldOptionResolver(FieldOptionRepository fieldOptionRepository, FieldRepository fieldRepository) {
        this.fieldOptionRepository = fieldOptionRepository;
        this.fieldRepository = fieldRepository;
    }

    public Field getField(FieldOption fieldOption) {
        return fieldRepository.getOne(fieldOption.getField().getId());
    }
}