package com.hendisantika.hris.springboothrisdemo.bundles.FieldOptionGroupSetBundle;

import com.hendisantika.hris.springboothrisdemo.core.main.ModelMutation;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-25
 * Time: 07:10
 */
@Component
public class FieldOptionGroupSetMutation extends ModelMutation<FieldOptionGroupSet> {

    public FieldOptionGroupSetMutation(FieldOptionGroupSetRepository fieldOptionGroupSetRepository) {
        this.fieldOptionGroupSetRepository = fieldOptionGroupSetRepository;
    }

    public FieldOptionGroupSet newFieldOptionGroupSet(String uid, String name, String description) {
        FieldOptionGroupSet fieldOptionGroupSet = new FieldOptionGroupSet(uid, name, description);

        fieldOptionGroupSetRepository.save(fieldOptionGroupSet);
        return fieldOptionGroupSet;
    }

    public Boolean deleteFieldOptionGroupSet(Long id) {
        return deleteModel(id, fieldOptionGroupSetRepository);
    }

    public FieldOptionGroupSet updateFieldOptionGroupSet(Long id, String uid, String name, String description) {
        FieldOptionGroupSet fieldOptionGroupSet = fieldOptionGroupSetRepository.getOne(id);

        if (uid != null)
            fieldOptionGroupSet.setUid(uid);

        if (name != null)
            fieldOptionGroupSet.setName(name);

        if (description != null)
            fieldOptionGroupSet.setDescription(description);

        fieldOptionGroupSetRepository.save(fieldOptionGroupSet);
        return fieldOptionGroupSet;
    }
}