package com.hendisantika.hris.springboothrisdemo.bundles.FriendlyReportBundle;

import com.hendisantika.hris.springboothrisdemo.core.main.ModelRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-22
 * Time: 22:04
 */
@Repository
public interface FriendlyReportRepository extends ModelRepository<FriendlyReport> {
}