package com.hendisantika.hris.springboothrisdemo.bundles.ArithmeticFilterBundle;

import com.hendisantika.hris.springboothrisdemo.bundles.FriendlyReportBundle.FriendlyReportRepository;
import com.hendisantika.hris.springboothrisdemo.core.main.ModelMutation;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-23
 * Time: 07:20
 */
@Component
public class ArithmeticFilterMutation extends ModelMutation<ArithmeticFilter> {

    public ArithmeticFilterMutation(ArithmeticFilterRepository arithmeticFilterRepository, FriendlyReportRepository friendlyReportRepository) {
        this.arithmeticFilterRepository = arithmeticFilterRepository;
        this.friendlyReportRepository = friendlyReportRepository;
    }

    public ArithmeticFilter newArithmeticFilter(String uid, String name, String description, String operator, String leftexpression, String rightexpression, Long friendlyReportId) {
        ArithmeticFilter arithmeticFilter = new ArithmeticFilter(uid, name, description, operator, leftexpression, rightexpression);

        if (friendlyReportId != null)
            arithmeticFilter.getFriendlyReports().add(friendlyReportRepository.getOne(friendlyReportId));

        arithmeticFilterRepository.save(arithmeticFilter);
        return arithmeticFilter;
    }

    public Boolean deleteArithmeticFilter(Long id) {
        return deleteModel(id, arithmeticFilterRepository);
    }

    public ArithmeticFilter updateArithmeticFilter(Long id, String uid, String name, String description, String operator, String leftexpression, String rightexpression, Long friendlyReportId) {
        ArithmeticFilter arithmeticFilter = arithmeticFilterRepository.getOne(id);

        if (uid != null)
            arithmeticFilter.setUid(uid);

        if (name != null)
            arithmeticFilter.setName(name);

        if (description != null)
            arithmeticFilter.setDescription(description);

        if (operator != null)
            arithmeticFilter.setOperator(operator);

        if (leftexpression != null)
            arithmeticFilter.setLeftexpression(leftexpression);

        if (rightexpression != null)
            arithmeticFilter.setRightexpression(rightexpression);

        if (friendlyReportId != null)
            arithmeticFilter.getFriendlyReports().add(friendlyReportRepository.getOne(friendlyReportId));

        return arithmeticFilter;
    }
}