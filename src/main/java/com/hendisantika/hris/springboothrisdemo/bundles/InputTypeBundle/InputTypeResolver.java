package com.hendisantika.hris.springboothrisdemo.bundles.InputTypeBundle;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.hendisantika.hris.springboothrisdemo.bundles.FieldBundle.FieldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-27
 * Time: 07:51
 */
@Component
public class InputTypeResolver implements GraphQLResolver<InputType> {
    @Autowired
    InputTypeRepository inputTypeRepository;

    @Autowired
    private FieldRepository fieldRepository;

    public InputTypeResolver(InputTypeRepository inputTypeRepository, FieldRepository fieldRepository) {
        this.inputTypeRepository = inputTypeRepository;
        this.fieldRepository = fieldRepository;
    }

}