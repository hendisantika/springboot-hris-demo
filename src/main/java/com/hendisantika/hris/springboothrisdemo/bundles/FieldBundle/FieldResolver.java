package com.hendisantika.hris.springboothrisdemo.bundles.FieldBundle;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.hendisantika.hris.springboothrisdemo.bundles.DataTypeBundle.DataTypeRepository;
import com.hendisantika.hris.springboothrisdemo.bundles.FieldGroupBundle.FieldGroupRepository;
import com.hendisantika.hris.springboothrisdemo.bundles.InputTypeBundle.InputType;
import com.hendisantika.hris.springboothrisdemo.bundles.InputTypeBundle.InputTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-24
 * Time: 07:22
 */
@Component
public class FieldResolver implements GraphQLResolver<Field> {
    @Autowired
    private FieldRepository fieldRepository;

    @Autowired
    private FieldGroupRepository fieldGroupRepository;

    @Autowired
    private InputTypeRepository inputTypeRepository;

    @Autowired
    private DataTypeRepository dataTypeRepository;

    public FieldResolver(FieldRepository fieldRepository, FieldGroupRepository fieldGroupRepository, InputTypeRepository inputTypeRepository, DataTypeRepository dataTypeRepository) {
        this.fieldRepository = fieldRepository;
        this.fieldGroupRepository = fieldGroupRepository;
        this.inputTypeRepository = inputTypeRepository;
        this.dataTypeRepository = dataTypeRepository;
    }

    public InputType getInputType(Field field) {
        return inputTypeRepository.getOne(field.getInputType().getId());
    }

}

