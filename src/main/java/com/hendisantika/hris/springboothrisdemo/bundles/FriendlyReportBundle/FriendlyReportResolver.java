package com.hendisantika.hris.springboothrisdemo.bundles.FriendlyReportBundle;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-26
 * Time: 07:16
 */
@Component
public class FriendlyReportResolver implements GraphQLResolver<FriendlyReport> {

}