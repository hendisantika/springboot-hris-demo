package com.hendisantika.hris.springboothrisdemo.bundles.FormBundle;

import com.hendisantika.hris.springboothrisdemo.core.common.ModelSpecification;
import com.hendisantika.hris.springboothrisdemo.core.main.ModelQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-26
 * Time: 07:12
 */
@Component
public class FormQuery extends ModelQuery<Form> {
    @Autowired
    private FormRepository formRepository;

    private ModelSpecification spec;

    public FormQuery(FormRepository formRepository) {
        this.formRepository = formRepository;
    }

    public Form getFormById(Long id) {
        return formRepository.getOne(id);
    }

    public List<Form> Forms(String where, String sort) {
        return query(where, spec, formRepository, sort);
    }
}