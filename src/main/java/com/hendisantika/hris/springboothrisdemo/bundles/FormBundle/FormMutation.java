package com.hendisantika.hris.springboothrisdemo.bundles.FormBundle;

import com.hendisantika.hris.springboothrisdemo.core.main.ModelMutation;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-26
 * Time: 07:12
 */
@Component
public class FormMutation extends ModelMutation<Form> {

    public FormMutation(FormRepository formRepository) {
        this.formRepository = formRepository;
    }

    public Form newForm(String uid, String name, String hypertext, String title) {
        Form form = new Form(uid, name, hypertext, title);

        formRepository.save(form);
        return form;
    }

    public Boolean deleteForm(Long id) {
        return deleteModel(id, formRepository);
    }

    public Form updateForm(Long id, String uid, String name, String hypertext, String title) {
        Form form = formRepository.getOne(id);

        if (uid != null)
            form.setUid(uid);

        if (name != null)
            form.setName(name);

        if (hypertext != null)
            form.setHypertext(hypertext);

        if (title != null)
            form.setTitle(title);

        return form;
    }
}