package com.hendisantika.hris.springboothrisdemo.bundles.FieldOptionGroupSetBundle;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-25
 * Time: 07:12
 */
@Component
public class FieldOptionGroupSetResolver implements GraphQLResolver<FieldOptionGroupSet> {
    @Autowired
    private FieldOptionGroupSetRepository fieldOptionGroupSetRepository;

    public FieldOptionGroupSetResolver(FieldOptionGroupSetRepository fieldOptionGroupSetRepository) {
        this.fieldOptionGroupSetRepository = fieldOptionGroupSetRepository;
    }
}