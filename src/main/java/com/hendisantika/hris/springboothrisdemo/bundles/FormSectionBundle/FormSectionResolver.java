package com.hendisantika.hris.springboothrisdemo.bundles.FormSectionBundle;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-26
 * Time: 07:14
 */
@Component
public class FormSectionResolver implements GraphQLResolver<FormSection> {
    @Autowired
    private FormSectionRepository formSectionRepository;

    public FormSectionResolver(FormSectionRepository formSectionRepository) {
        this.formSectionRepository = formSectionRepository;
    }
}