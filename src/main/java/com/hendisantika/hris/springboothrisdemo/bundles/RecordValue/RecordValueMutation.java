package com.hendisantika.hris.springboothrisdemo.bundles.RecordValue;

import com.hendisantika.hris.springboothrisdemo.core.main.ModelMutation;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-27
 * Time: 07:53
 */
@Component
public class RecordValueMutation extends ModelMutation<RecordValue> {

    public RecordValueMutation(RecordValueRepository recordValueRepository) {
        this.recordValueRepository = recordValueRepository;
    }

    public RecordValue newRecordValue(Long recordId, Long fieldId, String value) {
        RecordValue recordValue = new RecordValue(value);

        if (recordId != null)
            recordValue.setRecord(recordRepository.getOne(recordId));

        if (fieldId != null)
            recordValue.setField(fieldRepository.getOne(fieldId));

        recordValueRepository.save(recordValue);
        return recordValue;
    }

    public Boolean deleteRecordValue(Long id) {
        deleteModel(id, recordValueRepository);

        return true;
    }

    public RecordValue updateRecordValue(Long id, Long recordId, Long fieldId, String value) {
        RecordValue recordValue = recordValueRepository.getOne(id);

        if (value != null)
            recordValue.setValue(value);

        if (recordId != null)
            recordValue.setRecord(recordRepository.getOne(recordId));

        if (fieldId != null)
            recordValue.setField(fieldRepository.getOne(fieldId));

        recordValueRepository.save(recordValue);
        return recordValue;
    }

}
