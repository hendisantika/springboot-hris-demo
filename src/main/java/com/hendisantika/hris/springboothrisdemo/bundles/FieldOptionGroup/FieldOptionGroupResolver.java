package com.hendisantika.hris.springboothrisdemo.bundles.FieldOptionGroup;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.hendisantika.hris.springboothrisdemo.bundles.FieldBundle.Field;
import com.hendisantika.hris.springboothrisdemo.bundles.FieldBundle.FieldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-25
 * Time: 07:10
 */
@Component
public class FieldOptionGroupResolver implements GraphQLResolver<FieldOptionGroup> {
    @Autowired
    private FieldOptionGroupRepository fieldOptionGroupRepository;

    @Autowired
    private FieldRepository fieldRepository;

    public FieldOptionGroupResolver(FieldOptionGroupRepository fieldOptionGroupRepository) {
        this.fieldOptionGroupRepository = fieldOptionGroupRepository;
    }

    public Field getField(FieldOptionGroup fieldOptionGroup) {
        return fieldRepository.getOne(fieldOptionGroup.getField().getId());
    }

}
