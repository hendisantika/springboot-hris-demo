package com.hendisantika.hris.springboothrisdemo.bundles.FormBundle;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-26
 * Time: 07:13
 */
@Component
public class FormResolver implements GraphQLResolver<Form> {
    @Autowired
    private FormRepository formRepository;

    public FormResolver(FormRepository formRepository) {
        this.formRepository = formRepository;
    }
}