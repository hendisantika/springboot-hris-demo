package com.hendisantika.hris.springboothrisdemo.bundles.FieldGroupBundle;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.hendisantika.hris.springboothrisdemo.bundles.FieldBundle.FieldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-24
 * Time: 07:23
 */
@Component
public class FieldGroupResolver implements GraphQLResolver<FieldGroup> {
    @Autowired
    private FieldGroupRepository fieldGroupRepository;

    @Autowired
    private FieldRepository fieldRepository;

    public FieldGroupResolver(FieldGroupRepository fieldGroupRepository, FieldRepository fieldRepository) {
        this.fieldGroupRepository = fieldGroupRepository;
        this.fieldRepository = fieldRepository;
    }

}