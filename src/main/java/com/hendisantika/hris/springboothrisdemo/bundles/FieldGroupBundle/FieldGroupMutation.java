package com.hendisantika.hris.springboothrisdemo.bundles.FieldGroupBundle;

import com.hendisantika.hris.springboothrisdemo.bundles.FieldBundle.FieldRepository;
import com.hendisantika.hris.springboothrisdemo.core.main.ModelMutation;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-24
 * Time: 07:23
 */
@Component
public class FieldGroupMutation extends ModelMutation<FieldGroup> {

    public FieldGroupMutation(FieldGroupRepository fieldGroupRepository, FieldRepository fieldRepository) {
        this.fieldGroupRepository = fieldGroupRepository;
        this.fieldRepository = fieldRepository;
    }

    public FieldGroup newFieldGroup(String uid, String name, String description, Long field) {
        FieldGroup fieldGroup = new FieldGroup(uid, name, description);

        if (field != null) {
            fieldGroup.getFields().add(fieldRepository.getOne(field));
        }

        fieldGroupRepository.save(fieldGroup);
        return fieldGroup;
    }

    public Boolean deleteFieldGroup(Long id) {
        return deleteModel(id, fieldGroupRepository);
    }

    public FieldGroup updateFieldGroup(Long id, String uid, String name, String description, Long field) {
        FieldGroup fieldGroup = fieldGroupRepository.getOne(id);

        if (uid != null)
            fieldGroup.setUid(uid);

        if (name != null)
            fieldGroup.setName(name);

        if (description != null)
            fieldGroup.setDescription(description);

        //if (field != null) {
        fieldGroup.getFields().add(fieldRepository.getOne(field));
        //}

        fieldGroupRepository.save(fieldGroup);
        return fieldGroup;
    }

}
