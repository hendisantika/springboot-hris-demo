package com.hendisantika.hris.springboothrisdemo.bundles.DataTypeBundle;

import com.hendisantika.hris.springboothrisdemo.bundles.FieldBundle.Field;
import com.hendisantika.hris.springboothrisdemo.core.main.Model;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-22
 * Time: 22:00
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "dataType")
public class DataType extends Model {
    private String name;
    private String description;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "field_id", nullable = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Field field;

    public DataType() {
        super();
    }

    public DataType(Long id) {
        super();
        this.id = id;
    }

    public DataType(String name, String description) {
        super();
        this.name = name;
        this.description = description;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    @Basic
    @Column(name = "uid")
    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
