package com.hendisantika.hris.springboothrisdemo.bundles.FieldOptionGroup;

import com.hendisantika.hris.springboothrisdemo.bundles.FieldOptionBundle.FieldOptionRepository;
import com.hendisantika.hris.springboothrisdemo.core.main.ModelMutation;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-25
 * Time: 07:09
 */
@Component
public class FieldOptionGroupMutation extends ModelMutation<FieldOptionGroup> {


    public FieldOptionGroupMutation(FieldOptionGroupRepository fieldOptionGroupRepository, FieldOptionRepository fieldOptionRepository) {
        this.fieldOptionGroupRepository = fieldOptionGroupRepository;
        this.fieldOptionRepository = fieldOptionRepository;
    }

    public FieldOptionGroup newFieldOptionGroup(String uid, String name, String description, String operator, Long field, Long fieldOptionId) {
        FieldOptionGroup fieldOptionGroup = new FieldOptionGroup(uid, name, description, operator, field);


        if (fieldOptionId != null)
            fieldOptionGroup.getFieldOptions().add(fieldOptionRepository.getOne(fieldOptionId));

        fieldOptionGroupRepository.save(fieldOptionGroup);
        return fieldOptionGroup;
    }

    public Boolean deleteFieldOptionGroup(Long id) {
        return deleteModel(id, fieldOptionGroupRepository);
    }


    public FieldOptionGroup updateFieldOptionGroup(Long id, String uid, String name, String description, String operator, Long field, Long fieldOptionId) {
        FieldOptionGroup fieldOptionGroup = fieldOptionGroupRepository.getOne(id);


        if (uid != null)
            fieldOptionGroup.setUid(uid);

        if (name != null)
            fieldOptionGroup.setName(name);

        if (description != null)
            fieldOptionGroup.setDescription(description);


        if (operator != null)
            fieldOptionGroup.setOperator(operator);

        if (field != null)
            fieldOptionGroup.setField(field);

        if (fieldOptionId != null & !fieldOptionGroup.getFieldOptions().contains(fieldOptionRepository.getOne(fieldOptionId)))
            fieldOptionGroup.getFieldOptions().add(fieldOptionRepository.getOne(fieldOptionId));

        fieldOptionGroupRepository.save(fieldOptionGroup);
        return fieldOptionGroup;
    }

}