package com.hendisantika.hris.springboothrisdemo.bundles.UserBundle;

import com.hendisantika.hris.springboothrisdemo.core.common.ModelSpecification;
import com.hendisantika.hris.springboothrisdemo.core.main.ModelQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-28
 * Time: 08:19
 */
@Component
public class UserQuery extends ModelQuery<User> {
    @Autowired
    protected UserRepository userRepository;
    private ModelSpecification<User> spec;


    public UserQuery(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUserById(Long id) {
        return userRepository.getOne(id);
    }

    public List<User> Users(String where, String sort) {
        return query(where, spec, userRepository, sort);
    }

}