package com.hendisantika.hris.springboothrisdemo.bundles.ResourceBundle;

import com.hendisantika.hris.springboothrisdemo.core.main.Model;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-22
 * Time: 19:43
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "resource")
public class Resource extends Model {
    //    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
    private String name;
    private String description;
    private Boolean isGenerating;

    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date lastGenerated;

    private String messageLog;


    public Resource() {
        super();
    }

    public Resource(String uid, String name, String description, Boolean isGenerating, String messageLog) {
        super();
        this.name = name;
        this.description = description;
        this.isGenerating = isGenerating;
        this.messageLog = messageLog;
    }


    @Basic
    @Column(name = "uid")
    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "isGenerating")
    public Boolean getIsGenerating() {
        return isGenerating;
    }

    public void setIsGenerating(Boolean isGenerating) {
        this.isGenerating = isGenerating;
    }

    @Basic
    @Column(name = "lastGenerated")
    public Date getLastGenerated() {
        return lastGenerated;
    }

    public void setLastGenerated(Date lastGenerated) {
        this.lastGenerated = lastGenerated;
    }

    @Basic
    @Column(name = "messageLog")
    public String getMessageLog() {
        return messageLog;
    }

    public void setMessageLog(String messageLog) {
        this.messageLog = messageLog;
    }


}
