package com.hendisantika.hris.springboothrisdemo.bundles.DataTypeBundle;

import com.hendisantika.hris.springboothrisdemo.core.main.ModelMutation;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-23
 * Time: 07:25
 */
@Component
public class DataTypeMutation extends ModelMutation<DataType> {

    public DataTypeMutation(DataTypeRepository dataTypeRepository) {
        this.dataTypeRepository = dataTypeRepository;
    }

    public DataType newDataType(String name, String description, Long fieldId) {
        DataType dataType = new DataType(name, description);

//        if (fieldId != null)
//            dataType.getFields().add(fieldRepository.getOne(fieldId));
        if (fieldId != null)
            dataType.setField(fieldRepository.getOne(fieldId));

        dataTypeRepository.save(dataType);
        return dataType;
    }

    public Boolean deleteDataType(Long id) {
        return deleteModel(id, dataTypeRepository);
    }

    public DataType updateDataType(Long id, String uid, String name, String description, Long fieldId) {
        DataType dataType = dataTypeRepository.getOne(id);

        if (uid != null)
            dataType.setUid(uid);

        if (name != null)
            dataType.setName(name);

        if (description != null)
            dataType.setDescription(description);

//        if (fieldId != null)
//            dataType.getFields().add(fieldRepository.getOne(fieldId));
        if (fieldId != null)
            dataType.setField(fieldRepository.getOne(fieldId));

        dataTypeRepository.save(dataType);
        return dataType;
    }
}
