package com.hendisantika.hris.springboothrisdemo.bundles.FormSectionBundle;

import com.hendisantika.hris.springboothrisdemo.core.main.ModelMutation;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-26
 * Time: 07:13
 */
@Component
public class FormSectionMutation extends ModelMutation<FormSection> {

    public FormSectionMutation(FormSectionRepository formSectionRepository) {
        this.formSectionRepository = formSectionRepository;
    }

    public FormSection newFormSection(Integer formId, String uid, String name, String description) {
        FormSection formSection = new FormSection(formId, uid, name, description);

        formSectionRepository.save(formSection);
        return formSection;
    }

    public Boolean deleteFormSection(Long id) {
        return deleteModel(id, formSectionRepository);
    }

    public FormSection updateFormSection(Long id, Integer formId, String uid, String name, String description) {
        FormSection formSection = formSectionRepository.getOne(id);

        if (uid != null)
            formSection.setUid(uid);

        if (name != null)
            formSection.setName(name);

        if (description != null)
            formSection.setDescription(description);

        if (formId != null)
            formSection.setFormId(formId);

        return formSection;
    }
}