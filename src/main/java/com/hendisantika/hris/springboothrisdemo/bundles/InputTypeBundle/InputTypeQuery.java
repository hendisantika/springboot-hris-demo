package com.hendisantika.hris.springboothrisdemo.bundles.InputTypeBundle;

import com.hendisantika.hris.springboothrisdemo.core.common.ModelSpecification;
import com.hendisantika.hris.springboothrisdemo.core.main.ModelQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-27
 * Time: 07:51
 */
@Component
public class InputTypeQuery extends ModelQuery<InputType> {
    @Autowired
    InputTypeRepository inputTypeRepository;

    private ModelSpecification<InputType> spec;

    public InputTypeQuery(InputTypeRepository inputTypeRepository) {
        this.inputTypeRepository = inputTypeRepository;
    }

    public InputType getInputTypeById(Long id) {
        return inputTypeRepository.getOne(id);
    }

    public List<InputType> InputType(String where, String sort) {
        return query(where, spec, inputTypeRepository, sort);
    }
}