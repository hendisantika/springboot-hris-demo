package com.hendisantika.hris.springboothrisdemo.bundles.FieldOptionMergeBundle;

import com.hendisantika.hris.springboothrisdemo.core.main.Model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-23
 * Time: 07:08
 */
@Entity
@Table(name = "fieldOptionMerge")
public class FieldOptionMerge extends Model {
    private Integer fieldId;
    private Integer mergedfieldoptionId;
    private String removedfieldoptionvalue;
    private String removedfieldoptionuid;

    public FieldOptionMerge() {
        super();
    }

    public FieldOptionMerge(Long id) {
        super();
        this.id = id;
    }

    public FieldOptionMerge(Integer fieldId, Integer mergedfieldoptionId, String uid, String removedfieldoptionvalue, String removedfieldoptionuid) {
        super();
        this.fieldId = fieldId;
        this.mergedfieldoptionId = mergedfieldoptionId;
        this.removedfieldoptionvalue = removedfieldoptionvalue;
        this.removedfieldoptionuid = removedfieldoptionuid;
    }

    @Basic
    @Column(name = "field_id")
    public Integer getFieldId() {
        return fieldId;
    }

    public void setFieldId(Integer fieldId) {
        this.fieldId = fieldId;
    }

    @Basic
    @Column(name = "mergedfieldoption_id")
    public Integer getMergedfieldoptionId() {
        return mergedfieldoptionId;
    }

    public void setMergedfieldoptionId(Integer mergedfieldoptionId) {
        this.mergedfieldoptionId = mergedfieldoptionId;
    }

    @Basic
    @Column(name = "uid")
    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @Basic
    @Column(name = "removedfieldoptionvalue")
    public String getRemovedfieldoptionvalue() {
        return removedfieldoptionvalue;
    }

    public void setRemovedfieldoptionvalue(String removedfieldoptionvalue) {
        this.removedfieldoptionvalue = removedfieldoptionvalue;
    }

    @Basic
    @Column(name = "removedfieldoptionuid")
    public String getRemovedfieldoptionuid() {
        return removedfieldoptionuid;
    }

    public void setRemovedfieldoptionuid(String removedfieldoptionuid) {
        this.removedfieldoptionuid = removedfieldoptionuid;
    }

}
