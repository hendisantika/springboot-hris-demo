package com.hendisantika.hris.springboothrisdemo.bundles.RecordBundle;

import com.coxautodev.graphql.tools.GraphQLResolver;
import com.hendisantika.hris.springboothrisdemo.bundles.RecordValue.RecordValueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-27
 * Time: 07:53
 */
@Component
public class RecordResolver implements GraphQLResolver<Record> {
    @Autowired
    RecordValueRepository recordValueRepository;

    @Autowired
    RecordRepository recordRepository;

    public RecordResolver(RecordValueRepository recordValueRepository, RecordRepository recordRepository) {
        this.recordRepository = recordRepository;
        this.recordValueRepository = recordValueRepository;
    }
}