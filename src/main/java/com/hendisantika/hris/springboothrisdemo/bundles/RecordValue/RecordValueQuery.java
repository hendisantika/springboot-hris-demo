package com.hendisantika.hris.springboothrisdemo.bundles.RecordValue;

import com.hendisantika.hris.springboothrisdemo.core.common.ModelSpecification;
import com.hendisantika.hris.springboothrisdemo.core.main.ModelQuery;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-27
 * Time: 07:54
 */
@Component
public class RecordValueQuery extends ModelQuery<RecordValue> {
    private ModelSpecification<RecordValue> spec;

    public RecordValueQuery(RecordValueRepository recordValueRepository) {
        this.recordValueRepository = recordValueRepository;
    }

    public List<RecordValue> RecordValue(String where, String sort) {
        return query(where, spec, recordValueRepository, sort);
    }
}