package com.hendisantika.hris.springboothrisdemo.bundles.FormSectionBundle;

import com.hendisantika.hris.springboothrisdemo.core.main.Model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-23
 * Time: 07:14
 */
@Entity
@Table(name = "formSection")
public class FormSection extends Model {
    private Integer formId;
    private String name;
    private String description;

    public FormSection(Long id) {
        super();
        this.id = id;
    }

    public FormSection(Integer formId, String uid, String name, String description) {
        super();
        this.formId = formId;
        this.name = name;
        this.description = description;
    }

    public FormSection() {
        super();
    }

    @Basic
    @Column(name = "form_id")
    public Integer getFormId() {
        return formId;
    }

    public void setFormId(Integer formId) {
        this.formId = formId;
    }

    @Basic
    @Column(name = "uid")
    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
