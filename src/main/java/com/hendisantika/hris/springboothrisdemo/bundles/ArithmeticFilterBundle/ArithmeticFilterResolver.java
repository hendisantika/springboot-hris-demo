package com.hendisantika.hris.springboothrisdemo.bundles.ArithmeticFilterBundle;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-24
 * Time: 07:18
 */
@Component
public class ArithmeticFilterResolver implements GraphQLResolver<ArithmeticFilter> {
    @Autowired
    ArithmeticFilterRepository arithmeticFilterRepository;

    public ArithmeticFilterResolver(ArithmeticFilterRepository arithmeticFilterRepository) {
        this.arithmeticFilterRepository = arithmeticFilterRepository;
    }
}