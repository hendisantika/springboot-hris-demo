package com.hendisantika.hris.springboothrisdemo.bundles.FieldGroupBundle;

import com.hendisantika.hris.springboothrisdemo.bundles.FieldBundle.Field;
import com.hendisantika.hris.springboothrisdemo.core.main.Model;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-21
 * Time: 18:46
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "fieldGroup")
public class FieldGroup extends Model {
    private String name;
    private String description;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.MERGE
            },
            mappedBy = "fieldGroups")
    private Set<Field> fields = new HashSet();

    public FieldGroup() {
        super();
    }

    public FieldGroup(Long id) {
        super();
        this.id = id;
    }

    public FieldGroup(String uid, String name, String description) {
        super();
        this.name = name;
        this.description = description;
    }

    public Set<Field> getFields() {
        return fields;
    }

    public void setFields(Set<Field> fields) {
        this.fields = fields;
    }

    @Basic
    @Column(name = "uid")
    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
