package com.hendisantika.hris.springboothrisdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootHrisDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootHrisDemoApplication.class, args);
    }

}
