package com.hendisantika.hris.springboothrisdemo.core.main;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.hendisantika.hris.springboothrisdemo.core.common.ModelRepositories;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-23
 * Time: 07:21
 */
public class ModelMutation<T extends Model> extends ModelRepositories implements GraphQLMutationResolver {

    public Boolean deleteModel(Long id, ModelRepository<T> repository) {
        repository.delete(repository.getOne(id));
        return true;
    }
}