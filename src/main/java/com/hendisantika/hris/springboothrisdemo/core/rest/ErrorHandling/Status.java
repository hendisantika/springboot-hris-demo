package com.hendisantika.hris.springboothrisdemo.core.rest.ErrorHandling;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-22
 * Time: 19:36
 */
public enum Status {
    ERROR("ERROR"), OK("OK"), WARNING("WARNING");

    private String status;

    Status(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}