package com.hendisantika.hris.springboothrisdemo.core.rest.QueryStructure;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-21
 * Time: 20:16
 */
public enum Operator {
    EQ("eq"), NE("ne"),
    ILIKE("ilike"), NOTILIKE("!ilike"),
    LIKE("like"), NOTLIKE("!like"),
    GT("gt"), GE("ge"),
    LT("lt"), LE("le"),
    IN("in");

    private String op;

    Operator(String op) {
        this.op = op;
    }

    public String getOp() {
        return op;
    }
}