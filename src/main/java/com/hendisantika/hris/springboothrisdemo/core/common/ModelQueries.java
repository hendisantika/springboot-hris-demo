package com.hendisantika.hris.springboothrisdemo.core.common;

import com.hendisantika.hris.springboothrisdemo.bundles.ArithmeticFilterBundle.ArithmeticFilterQuery;
import com.hendisantika.hris.springboothrisdemo.bundles.DashboardChartBundle.DashboardChartQuery;
import com.hendisantika.hris.springboothrisdemo.bundles.DataTypeBundle.DataTypeQuery;
import com.hendisantika.hris.springboothrisdemo.bundles.FieldBundle.FieldQuery;
import com.hendisantika.hris.springboothrisdemo.bundles.FieldGroupBundle.FieldGroupQuery;
import com.hendisantika.hris.springboothrisdemo.bundles.FieldGroupSetBundle.FieldGroupSetQuery;
import com.hendisantika.hris.springboothrisdemo.bundles.FieldOptionBundle.FieldOptionQuery;
import com.hendisantika.hris.springboothrisdemo.bundles.FieldOptionGroupSetBundle.FieldOptionGroupSetQuery;
import com.hendisantika.hris.springboothrisdemo.bundles.FieldOptionMergeBundle.FieldOptionMergeQuery;
import com.hendisantika.hris.springboothrisdemo.bundles.FormBundle.FormQuery;
import com.hendisantika.hris.springboothrisdemo.bundles.FormSectionBundle.FormSectionQuery;
import com.hendisantika.hris.springboothrisdemo.bundles.FriendlyReportBundle.FriendlyReportQuery;
import com.hendisantika.hris.springboothrisdemo.bundles.InputTypeBundle.InputTypeQuery;
import com.hendisantika.hris.springboothrisdemo.bundles.RecordBundle.RecordQuery;
import com.hendisantika.hris.springboothrisdemo.bundles.RecordValue.RecordValueQuery;
import com.hendisantika.hris.springboothrisdemo.bundles.RelationalFilter.RelationalFilterQuery;
import com.hendisantika.hris.springboothrisdemo.bundles.ResourceBundle.ResourceQuery;
import com.hendisantika.hris.springboothrisdemo.bundles.UserBundle.UserQuery;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-28
 * Time: 08:21
 */
public class ModelQueries {
    @Autowired
    protected RecordQuery recordQuery;

    @Autowired
    protected UserQuery userQuery;

    @Autowired
    protected ResourceQuery resourceQuery;

    @Autowired
    protected RelationalFilterQuery relationalFilterQuery;

    @Autowired
    protected RecordValueQuery recordValueQuery;

    @Autowired
    protected InputTypeQuery inputTypeQuery;

    @Autowired
    protected FriendlyReportQuery friendlyReportQuery;

    @Autowired
    protected FormSectionQuery formSectionQuery;

    @Autowired
    protected FormQuery formQuery;

    @Autowired
    protected FieldOptionMergeQuery fieldOptionMergeQuery;

    @Autowired
    protected FieldGroupSetQuery fieldGroupSetQuery;

    @Autowired
    protected FieldOptionQuery fieldOptionQuery;

    @Autowired
    protected FieldGroupQuery fieldGroupQuery;

    @Autowired
    protected FieldOptionGroupSetQuery fieldOptionGroupSetQuery;

    @Autowired
    protected FieldQuery fieldQuery;

    @Autowired
    protected DataTypeQuery dataTypeQuery;

    @Autowired
    protected DashboardChartQuery dashboardChartQuery;

    @Autowired
    protected ArithmeticFilterQuery arithmeticFilterQuery;

}
