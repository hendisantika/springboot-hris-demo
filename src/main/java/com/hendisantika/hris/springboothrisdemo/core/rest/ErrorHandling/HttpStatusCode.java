package com.hendisantika.hris.springboothrisdemo.core.rest.ErrorHandling;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-22
 * Time: 19:35
 */
public enum HttpStatusCode {
    HTTP_STATUS_CODE_400("400"), HTTP_STATUS_CODE_403("403"), HTTP_STATUS_CODE_404("404");

    String httpStatusCode;

    HttpStatusCode(String httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    public String getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(String httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }
}