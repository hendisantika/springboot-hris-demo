package com.hendisantika.hris.springboothrisdemo.core.rest.QueryStructure;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-21
 * Time: 20:17
 */
public abstract class ApiOperation {
    protected String model;
    protected List<String> fields;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = StringUtils.capitalize(model);
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

}