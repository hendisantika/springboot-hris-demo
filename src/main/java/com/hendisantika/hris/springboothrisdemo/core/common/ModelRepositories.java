package com.hendisantika.hris.springboothrisdemo.core.common;

import com.hendisantika.hris.springboothrisdemo.bundles.ArithmeticFilterBundle.ArithmeticFilterRepository;
import com.hendisantika.hris.springboothrisdemo.bundles.DashboardChartBundle.DashboardChartRepository;
import com.hendisantika.hris.springboothrisdemo.bundles.DataTypeBundle.DataTypeRepository;
import com.hendisantika.hris.springboothrisdemo.bundles.FieldBundle.FieldRepository;
import com.hendisantika.hris.springboothrisdemo.bundles.FieldGroupBundle.FieldGroupRepository;
import com.hendisantika.hris.springboothrisdemo.bundles.FieldGroupSetBundle.FieldGroupSetRepository;
import com.hendisantika.hris.springboothrisdemo.bundles.FieldOptionBundle.FieldOptionRepository;
import com.hendisantika.hris.springboothrisdemo.bundles.FieldOptionGroup.FieldOptionGroupRepository;
import com.hendisantika.hris.springboothrisdemo.bundles.FieldOptionGroupSetBundle.FieldOptionGroupSetRepository;
import com.hendisantika.hris.springboothrisdemo.bundles.FieldOptionMergeBundle.FieldOptionMergeRepository;
import com.hendisantika.hris.springboothrisdemo.bundles.FormBundle.FormRepository;
import com.hendisantika.hris.springboothrisdemo.bundles.FormSectionBundle.FormSectionRepository;
import com.hendisantika.hris.springboothrisdemo.bundles.FriendlyReportBundle.FriendlyReportRepository;
import com.hendisantika.hris.springboothrisdemo.bundles.InputTypeBundle.InputTypeRepository;
import com.hendisantika.hris.springboothrisdemo.bundles.RecordBundle.RecordRepository;
import com.hendisantika.hris.springboothrisdemo.bundles.RecordValue.RecordValueRepository;
import com.hendisantika.hris.springboothrisdemo.bundles.RelationalFilter.RelationalFilterRepository;
import com.hendisantika.hris.springboothrisdemo.bundles.ResourceBundle.ResourceRepository;
import com.hendisantika.hris.springboothrisdemo.bundles.UserBundle.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-23
 * Time: 07:16
 */
public class ModelRepositories {
    @Autowired
    protected UserRepository userRepository;

    @Autowired
    protected ResourceRepository resourceRepository;
    @Autowired
    protected RelationalFilterRepository relationalFilterRepository;

    @Autowired
    protected FieldOptionRepository fieldOptionRepository;

    @Autowired
    protected FriendlyReportRepository friendlyReportRepository;

    @Autowired
    protected InputTypeRepository inputTypeRepository;

    @Autowired
    protected ArithmeticFilterRepository arithmeticFilterRepository;

    @Autowired
    protected FormSectionRepository formSectionRepository;

    @Autowired
    protected FormRepository formRepository;

    @Autowired
    protected FieldOptionMergeRepository fieldOptionMergeRepository;

    @Autowired
    protected FieldOptionGroupSetRepository fieldOptionGroupSetRepository;

    @Autowired
    protected FieldOptionGroupRepository fieldOptionGroupRepository;

    @Autowired
    protected FieldRepository fieldRepository;

    @Autowired
    protected FieldGroupSetRepository fieldGroupSetRepository;

    @Autowired
    protected FieldGroupRepository fieldGroupRepository;

    @Autowired
    protected DataTypeRepository dataTypeRepository;

    @Autowired
    protected DashboardChartRepository dashboardChartRepository;

    @Autowired
    protected RecordRepository recordRepository;

    @Autowired
    protected RecordValueRepository recordValueRepository;
}
