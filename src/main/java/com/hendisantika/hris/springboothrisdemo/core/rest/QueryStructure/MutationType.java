package com.hendisantika.hris.springboothrisdemo.core.rest.QueryStructure;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-21
 * Time: 20:17
 */
public enum MutationType {
    CREATE, DELETE, UPDATE;

    String mutationType;

    MutationType(String mutationType) {
        this.mutationType = mutationType;
    }

    MutationType() {
    }
}
