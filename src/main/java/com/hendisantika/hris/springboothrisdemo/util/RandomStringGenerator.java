package com.hendisantika.hris.springboothrisdemo.util;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-hris-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-04-21
 * Time: 18:35
 */
@Service
public class RandomStringGenerator {
    public String givenUsingApache_whenGeneratingRandomAlphabeticString_thenCorrect(int length) {

        String generatedString = RandomStringUtils.randomAlphanumeric(length);

        return generatedString;
    }
}